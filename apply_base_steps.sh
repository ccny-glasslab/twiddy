#
# apply_base_steps.sh
#
## Handle arguments
if [ $# -lt 2 ]; then
  echo "Missing two arguments: node number, e.g. 01, and node IP, e.g. 192.168.2.3"
  exit 1
fi

NODE_NUM="$1"
NODE_IP="$2"
cd /vagrant
bash -x ./step-1-set_hostname.sh ${NODE_NUM}
bash -x ./step-2-install_kubeadm.sh ${NODE_IP}
sudo bash -x ./step-3-install_docker.sh

# Adjust bashrc to hide full path of current dir
cp ./.bashrc ~
