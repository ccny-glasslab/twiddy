#
# step-2-install_kubeadm.sh
#
if [ $# -eq 0 ]; then
  echo "Missing argument for node IP address, e.g. 192.168.2.3"
  exit 1
fi

NODE_IP="$1"
sudo apt-get update && sudo apt-get install -y apt-transport-https curl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
cat <<EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF
sudo apt-get update
sudo apt-get install -y kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl

sudo cp /vagrant/10-kubeadm.conf /etc/systemd/system/kubelet.service.d
sudo sed s/\<IP_HERE\>/${NODE_IP}/ -i /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
