#
# step-1-set_hostname.sh
#
## Handle arguments
if [ $# -eq 0 ]; then
  echo "Missing argument for node number, e.g. 01"
  exit 1
fi

NODE_NUM="$1"
sudo sed s/ubuntu-bionic/node${NODE_NUM}/g -i /etc/hosts
sudo hostnamectl set-hostname node${NODE_NUM}
