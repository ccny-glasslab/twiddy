# twiddy

Assets from playing around with a Kubernetes cluster installed across
VirtualBox/Vagrant VM's on my laptop.

### Getting started

* Install VirtualBox and Vagrant
* Install Vagrant plugins:
    ```bash
    vagrant plugin install vagrant-vbguest vagrant-disksize
    ```
* Bring up VMs
    ```bash
    bash -x ./install.sh
    ```
* Last few lines of command output should be: "Your Kubernetes control-plane has initialized successfully!"
  * Configure Pod network add-on
      ```bash
      vagrant ssh node01
      # Paste the first three commands beginning with
      #    mkdir -p $HOME/.kube
      kubectl apply -f ./calico.yaml
      ```
  * Join nodes to the cluster
      ```bash
      vagrant ssh node02
      # Paste the one command beginning with 'kubeadm join ...'
      exit
      vagrant ssh node03
      # Paste that same onliner beginning with 'kubeadm join ...'
      ```
