#
# install.sh
#
vagrant up node01
vagrant ssh node01 -c 'bash -x /vagrant/apply_base_steps.sh 01 192.168.2.2'

vagrant up node02
vagrant ssh node02 -c 'bash -x /vagrant/apply_base_steps.sh 02 192.168.2.3'

vagrant up node03
vagrant ssh node03 -c 'bash -x /vagrant/apply_base_steps.sh 03 192.168.2.4'

vagrant ssh node01 -c 'bash -x /vagrant/step-4-init_cluster.sh'
