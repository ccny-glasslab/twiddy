#
# step-4-init_cluster.sh
#
sudo kubeadm init --pod-network-cidr 192.168.2.0/24 --apiserver-advertise-address=192.168.2.2

# Setup dashboard
# URL came from this doc: https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/#accessing-the-dashboard-ui
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-beta8/aio/deploy/recommended.yaml
kubectl apply -f /vagrant/dashboard_access.yaml
kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep admin-user | awk '{print $1}')

# Port forward into dashboard
# $ ssh -L 8001:127.0.0.1:8001 -N vagrant@192.168.2.2 -i .vagrant/machines/node01/virtualbox/private_key
# Visit page:
# http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy

# After install JH Helm chart, one pod was stuck. The K8s dashboard
# mentioned "pod has unbound immediate PersistentVolumeClaims"
